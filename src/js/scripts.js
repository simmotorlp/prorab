//= partials/jquery.formstyler.min.js
//= partials/slick.min.js
//= partials/jquery.mmenu.all.js

$(function () {

    $("#mobile_menu").mmenu({
        "extensions": [
            "pagedim-black",
            "position-front",
            "position-left",
            "multiline"
        ],
        "navbar": {
            "title": false
        },
        navbars: [{
            content: ['<a href="#" class="logo"><span class="main_logo">Прораб</span></a>']
        }],
        hooks: {
            "open:start": function ($panel) {
                $('.hamburger').addClass('is-active');
            },
            "close:start": function ($panel) {
                $('.hamburger').removeClass('is-active');
            }
        },
        clone: false,
        offCanvas: {
            pageSelector: ".mmenu_wrap"
        }
    });

    $('.js_input').styler();

    $('.mobile_search').click(function () {
        $('.js_site_search').show();
        $(document).mouseup(function (e) {
            var div = $(".js_site_search");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                div.hide();
            }
        });
    });

    $('.js_main_slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
    });

    $('.js_brands_slider').slick({
        dots: true,
        infinite: false,
        speed: 500,
        rows: 1,
        slidesPerRow: 4,
        prevArrow: '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    rows: 2,
                    slidesPerRow: 2,
                    infinite: false,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    rows: 1,
                    slidesPerRow: 1,
                    infinite: false,
                    dots: true
                }
            }
        ]
    });

    $('.js_product_slider').slick({
        dots: true,
        infinite: false,
        speed: 500,
        prevArrow: '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
        rows: 1,
        slidesPerRow: 4,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    rows: 2,
                    slidesPerRow: 2,
                    infinite: false,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    rows: 1,
                    slidesPerRow: 1,
                    infinite: false,
                    dots: true
                }
            }
        ]
    });

    $('.jq-selectbox__trigger-arrow').append('<i class="fa fa-chevron-down" aria-hidden="true"></i>');

    $('nav > ul > li > ul > .menu-item-has-children > a').click(function () {
        event.preventDefault();
    });

    $(".search_submit").on({
        mouseenter: function () {
            $(this).parent().find('.fa').css({'color': '#fad43a', 'opacity': 1});
        },
        mouseleave: function () {
            $(this).parent().find('.fa').css({'color': '#000', 'opacity': 0.4});
        }
    });

    $('ul.tabs_caption').on('click', 'li:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs_content').removeClass('active').eq($(this).index()).addClass('active');
    });

    $("a.scrollto").click(function () {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 1000);
        return false;
    });

    // $(document).on('change', '.quantity .qty', function () {
    //     $(this).parent('.quantity').next('.add_to_cart_button').attr('data-quantity', $(this).val());
    // });

    jQuery.ajaxSetup({complete: function(){jQuery(".js_input").styler(); }});

    $('.product_offer_slide .ajax_add_to_cart').html('Купить');

});
